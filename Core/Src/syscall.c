/*
 * syscall.c
 *
 *  Created on: Oct 23, 2021
 *      Author: legha
 */
#include "stm32f4xx.h"

int _write(int file, char *ptr, int len){
	for(int i=0; i<len; i++)
		ITM_SendChar((*ptr++));
	return len;
}
